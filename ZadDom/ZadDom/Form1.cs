﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZadDom
{
    
    public partial class Kopalnia : Form
    {
        private int Counter = 1;                // liczba naciśnięć przycisku pauzowania/startu w czasie trawnia programu
        private int timeCounter = 0;            // zmienna licząca czas
        private int moneyMaker = 0;             // zmienna licząca aktualny stan złota
        private int howManyWorkers = 0;         // zmienna przechowująca liczbę pracowników
        private int howManyWagons = 0;          // zmienna przechowująca liczbę wagonów
        private int money = 0;                  // zmienna licząca przychód złota na sekundę
        private int diamond = 0;                // zmienna licząca przychód diamentów na sekundę
        private int diamonds = 0;               // zmienna licząca aktualną ilość diamentów
        private int workerLevel = 1;            // poziom pracownika (im wyższy tym większy przychód)
        private int wagonLevel = 1;             // poziom wagonu (im wyższy tym większy przychód)
        private int riseOfWorkerSalary = 0;     // ilość podwyżek dla pracowników
        private int randomTime;                 // zmienna przechowująca wartość czasu losowej sytuacji (strajku robotników)
        private int randomCrashTime;            // zmienna przechowująca wartość czasu losowej awarii kopalni
        private int repair = 0;                 // ilość napraw kopalni po awariach

        //konstruktor
        public Kopalnia()
        {
            InitializeComponent();
            //wylosowanie czasu strajku między 30 a 60 sekundą każdej minuty
            randomTime = action(30, 60);
            //wylosowanie czasu awarii odbywającej się co 1,5 minuty
            randomCrashTime = action(60, 90);
        }
        //funckja losuje i zwraca liczbę należącą do podanego przedziału
         int action(int a, int b)
        {
            Random value = new Random();
            return value.Next(a, b);
        }
        //funckja odpowiedzialna za uruchomienie/pauzowanie gry
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if(Counter%2 == 1)
            {
                //rozpoczęcie gry
                timer.Start();
                buttonStart.Text = "Pauza";
                buttonStart.BackColor = Color.Red;
            }
            else
            {
                //pauza
                timer.Stop();
                buttonStart.Text = "Start";
                buttonStart.BackColor = Color.Green;
            }
            //inkrementacja licznika kliknięć 
            Counter++; 
        }

        //taktowanie zegara (interval 1000ms)
        private void timer_Tick(object sender, EventArgs e)
        {
            //inkrementacja licznika czasu co każdą sekundę
            timeCounter++;
            //zmienne przechowujące minuty i sekundy trwającej rozgrywki
            int minutes = timeCounter / 60;
            int seconds = timeCounter % 60;
            //zmiana czasu z liczby na string
            textBoxTime.Text = "0"+ minutes.ToString() + " : " + seconds.ToString();
            //przyrost złota co sekundę
            money = money + 2 + howManyWorkers * workerLevel + 2 * howManyWagons * wagonLevel;
            //aktualny stan złota
            moneyMaker = money - 30 * howManyWorkers - 50 * howManyWagons - howManyWorkers * riseOfWorkerSalary - 100 * repair;
            //zmiana z liczby na string
            textBoxMoney.Text = moneyMaker.ToString();
            //funkcja zmieniająca kolor przycisku oraz możliwość kliknięcia w niego gdy ilość zdobytego złota na to pozwala
            greenButtonToBuy();
            //przyznawanie diamentu co 6 sekund
            if(timeCounter%6==0)
            {
                diamond++;
            }
            //aktualny stan diamentów
            diamonds = diamond - (workerLevel/2) * 5 - (wagonLevel/2) * 10;
            //zmiana liczby diamentów na string
            textBoxDiamond.Text = diamonds.ToString();
            //funckja zmienijąca kolor przycisku oraz możliwość kliknięci w niego gdy ilość uzyskanych diamentów pozwala na wykonanie tej czynności
            buttonUpgradeEnable();
            //strajk
            if (timeCounter%60 == randomTime)
            {
                //zatrzymanie czasu
                timer.Stop();
                if (MessageBox.Show("Strajk robotników. Żądają podwyżki o 10$", "Strajk w kopalni", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                     {
                            //wzrost pensji
                            riseOfWorkerSalary += 10;
                     }
                else
                {
                    MessageBox.Show("Połowa robotników zwolniła się z powodu braku podwyżki","Zwolnienia");
                    //zmniejszenie liczby pracowników o połowę
                    howManyWorkers = howManyWorkers / 2;
                }
                //wznowienie czasu
                timer.Start();
            }
            //awaria
            if(timeCounter%90 == randomCrashTime)
            {
                timer.Stop();
                int o = (repair + 1) * 50;
                MessageBox.Show("Awaria kopalni. Koszty naprawy to: " + o.ToString() + "$", "Awaria");
                repair++;
                timer.Start();
            }
            //gra kończy się po zdobyciu 5000 złota
            if (moneyMaker >= 5000)
            {
                timer.Stop();
                MessageBox.Show("Gratulacje wygrałeś/aś. Twój czas to: 0" + timeCounter / 60 + ":" + timeCounter % 60, "Wygrana");
                Close();
            }
        }

        //funckja kończąca pracę programu po kliknięciu
        private void buttonExit_Click(object sender, EventArgs e)
        {
            moneyMaker = 0;
            Close();
        }

        //funkcja kupująca robotnika
        private void buttonBuyWorker_Click(object sender, EventArgs e)
        {
            if(moneyMaker >= 30 && Counter%2==0)
            {
              
                howManyWorkers++;
                textBoxHowManyWorkers.Text = howManyWorkers.ToString();
            }
           
          
        }

        //funkcja zmieniająca dostępność oraz kolor przycisków do kupowania
        void greenButtonToBuy()
        {
            if (moneyMaker >= 30)
            {
                buttonBuyWorker.Enabled = true;
                buttonBuyWorker.BackColor = Color.Gold;
            }
            else
            {
                buttonBuyWorker.Enabled = false;
                buttonBuyWorker.BackColor = Color.LightGray;
            }
                

            if (moneyMaker >= 50)
            {
                buttonBuyWagon.Enabled = true;
                buttonBuyWagon.BackColor = Color.Gold;
            }
            else
            {
                buttonBuyWagon.Enabled = false;
                buttonBuyWagon.BackColor = Color.LightGray;
            }
                
        }
        //funkcja kupująca wagon
        private void buttonBuyWagon_Click(object sender, EventArgs e)
        {
            if(moneyMaker >= 50 && Counter%2==0)
            {
                howManyWagons++;
                textBoxHowManyWagons.Text = howManyWagons.ToString();
            }
        }
        //funkcja ulepszająca robotników
        private void buttonWorkerUpgrade_Click(object sender, EventArgs e)
        {
            if(diamonds >=5 && workerLevel < 4)
            {
                workerLevel = 2 * workerLevel;
                pictureBoxWorker.ImageLocation = @"C:\Users\Wojtek\Documents\kredek\zadDom1\robotnik.jpg";
            }
            if(workerLevel == 2)
            {
                buttonWorkerUpgrade.Text = "Ostatnie ulepszenie";
            }
            if(workerLevel == 4)
            {
                buttonWorkerUpgrade.Text = "---";
                buttonWorkerUpgrade.BackColor = Color.LightGray;
                buttonWorkerUpgrade.Enabled = false;
            }
        }
        //funkcja ulepszająca wagony
        private void buttonWagonUpgrade_Click(object sender, EventArgs e)
        {
            if(diamonds >= 10 && wagonLevel <  4)
            {
                wagonLevel = 2* wagonLevel;
                pictureBoxWagon.ImageLocation = @"C:\Users\Wojtek\Documents\kredek\zadDom1\wagon.jpg";
            }
            if(wagonLevel == 2)
            {
                buttonWagonUpgrade.Text = "Ostatnie ulepszenie";
            }
            if(wagonLevel == 4)
            {
                buttonWagonUpgrade.Text = "---";
                buttonWagonUpgrade.BackColor = Color.LightGray;
                buttonWagonUpgrade.Enabled = false;
            }
        }
        //funkcja zmieniająca dostępność oraz kolor przycisków do ulepszania
        void buttonUpgradeEnable()
        {
            if(diamonds < 5)
            {
                buttonWorkerUpgrade.Enabled = false;
            }
            else
            {
                buttonWorkerUpgrade.Enabled = true;
            }

            if(diamonds < 10)
            {
                buttonWagonUpgrade.Enabled = false;
            }
            else
            {
                buttonWagonUpgrade.Enabled = true;
            }
        }

       
    
    }
}
